# CHIP-2021-09 Group Tokenization Marking

        Title: Group Tokenization Marking
        First Submission Date: 2021-09-25
        Owners: Anonymous Contributor A60AB5450353F40E.
        Type: Technical
        Layer: Consensus
        Status: DRAFT
        Last Edit Date: 2021-11-11

## Summary

This proposal describes an upgrade to Bitcoin Cash Group Tokenization that would enable marking and unmarking BCH or token amounts so that they cease to be fungible with amounts of distinct or no marking.

This was known as "BALANCE_BCH" and "Subgroup" features in Andrew Stone's [original proposal](https://docs.google.com/document/d/1X-yrqBJNj6oGPku49krZqTMGNNEWnUJBRFjX7fJXvTs).
They're symmetrical features because one creates fungibility barriers between BCH, and the other creates them between group tokens.
The barriers are implemented by marking the UTXOs with an identifier, therefore the name.

The upgrade assumes that [Group Tokenization "base"](https://gitlab.com/0353F40E/group-tokenization/-/blob/master/CHIP-2021-02_Group_Tokenization_for_Bitcoin_Cash.md) is enabled.

## Motivation

This will enable creation of tokens backed by either BCH or group tokens.
Minting such tokens will require locking an equal amount of the underlying currency, and melting them will release the underlying currency.
Because minting and melting will be managed using the authority UTXO, it means a Script smart contract can control minting and melting.

This means that supply management could be handed to a public covenant, while tokens themselves could remain P2PKH and easily be transacted with.

## Technical Description

Transaction balancing rule is updated to disallow moving amounts between marked and unmarked outputs, or between outputs with distinct marks.

### Marked BCH

We will reuse the `tokenID` field to support marked BCH.
With marked BCH, the `tokenAmount` will be omitted for ordinary amounts.

Marked BCH ordinary output:  
*<satoshiAmount\><scriptLength\>* `[PFX_GROUP <markID>]` *<pubKeyScript\>*

Marked BCH authority output:  
*<satoshiAmount\><scriptLength\>* `[PFX_GROUP <markID><authorityFlags><leftToMint> <<metaLen1>[metaData1]... <metaLen5>[metaData5]>]` *<pubKeyScript\>*

Where `markID` is a 32-byte identfier.

We will distinguish tokens from marked BCH by a flag in the `markID`:

MARKED_BCH = 0x02.

For marked BCH, the 1st MSB of `markID` of will then be:

ONE_TOKEN_STANDARD & MARKED_BCH.

The GENESIS step is performed the same way as for tokens.

The `leftToMint` functions similarly, it puts a cap on how many satoshis can be marked and serves as a reverse counter of satoshi amounts marked.

### Marked Tokens

We will extend the token output annotation to be able to carry both the underlying `tokenID` and the `markID`:

Ordinary marked token output:  
*<satoshiAmount\><scriptLength\>* `[PFX_GROUP <[markID]tokenID><qty>]` *<pubKeyScript\>*

Authority marked token output:  
*<satoshiAmount\><scriptLength\>* `[PFX_GROUP <[markID]tokenID><0x00><authorityFlags><leftToMint> <<metaLen1>[metaData1]... <metaLen5>[metaData5]>]` *<pubKeyScript\>*

Where `markID` is a 32-byte identfier.

We will distinguish marked tokens by a flag in the `markID`:

MARKED_TOKENS = 0x04.

For marked tokens, the 1st MSB of `markID` of will then be:

ONE_TOKEN_STANDARD & MARKED_TOKENS.

Balancing rules for ordinary marked tokens remain the same, because enforcing balances for distinct `<[markID]tokenID>` field will produce the desired result: tokens with shared `tokenID` but different or null `markID` can't mix.

Minting a marked token will require a matching amount of parent token to be spent, and melting a marked token will allow reclaiming the parent token.

The GENESIS step is performed the same way as for tokens.

The marked token genesis output must satisfy:

`markID == H(genData)`,
`tokenQty == 0x00`,
`leftToMint > 0`,

where:  
`H` is the double SHA256 hash function and  
`genData` is byte concatenation of:

1. Hashes of each transaction input, ordered by their index and hashing only the following fields:
    - prevout transaction hash,
    - prevout index,
    - prevout satoshi amount,
    - prevout locking script,
2. Output index of the genesis output being generated,
3. All outputs ordered by their index and `tokenID` left out for genesis outputs.

The `tokenID` could be set to anything, but such genesis output would be useless because it can't mark tokens which don't exist, and we'd end up with an authority-type NFT.

If it would match an existing `tokenID`, it could convert amounts of tokens into marked tokens and vice versa.

The `leftToMint` functions similarly, it puts a cap on how many tokens can be marked and serves as a reverse counter of amounts marked.

## Copyright

To the extent possible under law, this work has waived all copyright and related or neighboring rights to this work under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
