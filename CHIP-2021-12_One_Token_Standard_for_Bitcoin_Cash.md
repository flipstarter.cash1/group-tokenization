# CHIP-2021-12 One Token Standard for Bitcoin Cash

        Title: One Token Standard for Bitcoin Cash
        First Submission Date: 2021-12-04
        Owners: Anonymous Contributor A60AB5450353F40E
        Type: Technical
        Layers: Wallet
        Status: DRAFT
        Current Version: 0.1
        Last Edit Date: 2021-12-04

## Summary

This proposal describes a standard way to create [native tokens](https://gitlab.com/0353F40E/group-tokenization/-/blob/master/CHIP-2021-02_Group_Tokenization_for_Bitcoin_Cash.md#native-token-group) by specifying
contents of the token genesis transaction.
Apart from the native token's genesis output used to mint the token supply, this proposal specifies auxiliary outputs which will use Script to implement extended features: detached owner, mint pool, and metadata.

## Genesis Transaction Specification

Wallets receiving some new token should walk the chain back to the token's genesis transactions and inspect it to determine the token's type.
Tokens matching the below token genesis standard will be considered one-token-standard (OTS) tokens.
From there, wallets will be able to automatically obtain current metadata and token supply information.

The token's genesis output must match the OTS token genesis specification:

1. The token's native `groupType` must be one of the 2 native token types:
    - `NATIVE_TOKEN == 0x1000` for NFTs, or
    - `NATIVE_TOKEN | HAS_GROUP_AMOUNT == 0x1001` for fungible tokens.
2. The token's genesis output must be a singularity output i.e. `groupAmount == 0`.
3. The contract placed on token's genesis output must:
    - Prove that it's been set at token's genesis.
    - Prove that it couldn't have been changed since genesis.
    - Prove that a singularity output couldn't have escaped the covenant.
    - Prove that the "detached owner" output has been spent at input index+1.
    - Prove that the "mintPool" output has been spent at input index+2 or that it is an NFT token.

The "detached owner" contract is defined as the contract which:

1. Had its genesis output created in the same genesis TX as the OTS genesis output and at index+1.
2. Of NFT token type (`NATIVE_TOKEN | HAS_GROUP_AMOUNT`).

The contents of the locking script are not specified so this contract can be P2PKH.

The "metadata" contract is defined as the contract which:

1. Had its genesis output created in the same genesis TX as the OTS genesis output and at index+3.
2. Of NFT token type (`NATIVE_TOKEN | HAS_GROUP_AMOUNT`).
3. Prove that the "owner" output has been spent at index+1.
4. OP_DROP

The "mintPool" contract is defined as the contract which:

1. Had its genesis output created in the same genesis TX as the OTS genesis output and at index+2.
2. Of "carried digest" group type, with an amount field (`HAS_GROUP_AMOUNT`).
3. Prove that the "owner" output has been spent at index-1.
4. Prove that exactly 1 "change" output is created.
5. Prove that the "change" groupAmount is `<=` the output being spent.
6. Prove that the sum of OTS group outputs balances with its `groupAmount` states.

Any token genesis transactions having the above contract structure will be considered an "one token standard" token.

An example transaction graph is presented below:

<p align="center"><img src="./ots.png"></img></p>

Because "owner" and "metadata" contracts must be placed on an NFT group their individual output graphs will always form a chain, so wallets can retrieve the current token state just by following the 2 chains.
The "mintPool" contract will also be a chain, but that will be enforced by its Script since it will not be a NFT group type.

## OTS Contract Definitions

### OTS Baton

### OTS Owner

### OTS MintPool

### OTS Metadata