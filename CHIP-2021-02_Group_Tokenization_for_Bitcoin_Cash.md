# CHIP-2021-02 Unforgeable Groups for Bitcoin Cash

        Title: Unforgeable Groups for Bitcoin Cash
        First Submission Title: Group Tokenization for Bitcoin Cash
        First Submission Date: 2021-02-23
        Owners: Anonymous Contributor A60AB5450353F40E
        Type: Technical
        Layers: Consensus, Network
        Status: DRAFT
        Current Version: 4.1
        Last Edit Date: 2021-12-08

## Contents

1. [Summary](#summary)
2. [Deployment](#deployment)
3. [Benefits](#benefits)
4. [Technical Description](#technical-description)
5. [Specification](#specification)
6. [Appendix - Group Logic Pseudo Code](#appendix-group-logic-pseudo-code)
7. [Appendix - Group Logic Flowchart](#appendix-group-logic-flowchart)
8. [Appendix - Example Uses](#appendix-example-uses)
9. [Activation Costs and Risks](#activation-costs-and-risks)
10. [Ongoing Costs and Risks](#ongoing-costs-and-risks)
11. [Implementations](#implementations)
12. [Test Cases](#test-cases)
13. [Security Considerations](#security-considerations)
14. [Evaluation of Alternatives](#evaluation-of-alternatives)
15. [Stakeholder Cost and Benefits Analysis](#stakeholder-cost-and-benefits-analysis)
16. [Statements](#statements)
17. [Future Work](#future-work)
18. [Discussions](#discussions)
19. [Changelog](#changelog)
20. [Copyright](#copyright)
21. [Credits](#credits)

## Summary

[[Back to Contents](#contents)]

This proposal describes an upgrade to Bitcoin Cash peer-to-peer electronic cash system that will allow Bitcoin Cash outputs to form unforgeable groups.
Bitcoin Cash output format will be extended to optionally carry a group identifier, where outputs with a common identifier will form a group.

Consensus rules will enforce that the indentifier set on the first member shall be a cryptographic commitment to the group's whole genesis transaction.
The identifier preimage will be structured to contain commitments to parts of the genesis transaction instead of raw data, which will allow contracts to make compact fixed-size proofs about group genesis transactions.

New outputs will be allowed into the group only by spending an existing output of the same group, allowing only members to introduce new members.
Therefore, every group will be exclusive to descendants of the first member, and every member will carry a commitment of its group's genesis transaction.
We will refer to these generic groups as "carried commitment" groups.
Group introspection opcodes will be introduced to allow contracts to access the commitment and make proofs about group genesis transactions.

Grouped outputs will be extended with an optional 8 byte field, giving contracts a way to store a small state with their UTXOs.
Group introspection opcodes will be introduced to allow contract to enforce how the state may be changed.

Consensus token logic will be introduced over some types of groups to enable native tokens, in effect enabling dual-currency outputs (BCH & token) which will be "free" P2PKH citizens of Bitcoin Cash blockchain.
At a minimum such outputs will always be weighted down by the BCH dust limit.
Consequentially, every token output will slightly increase demand for BCH and the incentive for users to consolidate the UTXO set will not be affected.

## Deployment

[[Back to Contents](#contents)]

This proposal targets May 2023 activation.
It has dependencies in:

- [CHIP-2021-02: Native Introspection Opcodes](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md), which will be extended by this proposal with additional opcodes.
- [Optional] [CHIP-2021-01-RSN: Ranged Script Number Format](https://github.com/bitjson/pmv3/blob/master/ranged-script-numbers.md), if using the "Ranged Script Number" (RSN) format instead of VarInt.

Optionally, it can be merged with:

- [CHIP-2021-01-PMv3: Version 3 Transaction Format](https://github.com/bitjson/pmv3), by extending the v3 transaction format with two new fields.
The ["detached proof"](https://github.com/bitjson/pmv3/tree/fbe89e0de877d773085c68a6d4e079bdb6ff63b0#detached-proofs) feature would be replaced by the ["carried digest"](#carried-digest-group) feature proposed here.

## Benefits

[[Back to Contents](#contents)]

### Carried Digest

In May 2022, Bitcoin Cash will see two major upgrades to the Script system:

- [CHIP-2021-02: Native Introspection Opcodes](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md) and
- [CHIP-2021-03: Bigger Script Integers](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Bigger-Script-Integers.md).

The two upgrades will enable powerful Bitcoin Cash covenants - contracts which validate forward.
For example, they can require the spender to prove he's returned some "change" to the same contract and release the rest only if some other condition is met.

Problem is, what if the contract needs to prove that original funds came from a specific transaction?
For example, implementing an NFT would require ability to make such proofs, otherwise it wouldn't be able to prove it hasn't been forged.
That class of contracts must also validate backwards, they need to be able to prove their ancestry.

With existing blockchain primitives, the only way to prove ancestry would require the contract to reconstruct its parent's TXID preimage as proof.
This would work because the TXID is a cryptographic digest of the parent transaction and the digest is accessible from Script local scope.
That is also the problem because the preimage is the whole parent transaction, not just the parts we want to prove about it.
Such proof would require embedding the entire parent TX as proof, and if the parent also had to prove the grandparent, then the proof would grow quadratically with each contract step.

One way to address the problem would be to tweak the TXID preimage format: have each input's proof be individually hashed when constructing the preimage.
This has been proposed as ["detached proof"](https://github.com/bitjson/pmv3/tree/fbe89e0de877d773085c68a6d4e079bdb6ff63b0#detached-proofs) and it would make [fixed size inductive proofs of ancestry](https://blog.bitjson.com/pmv3-build-decentralized-applications-on-bitcoin-cash/) possible.

Instead of modifying the existing TXID "digest", here we propose to introduce another "digest" primitive tailored for the task, which would make proofs simpler and smaller.
Additionally, we propose to introduce an optional 8-byte field that allow contracts to carry a state and compare it with that of the parent by using introspection opcodes.
The two fields would also lay the foundation for native tokens.

### Non Fungible Tokens

If only the proposed "carried digest" feature is enabled then it would become possible to implement NFTs using a Script contract.
Apart from proving its own unforgeability, the contract would have to verify that exactly one UTXO is being created.
Because Script doesn't support for-each loops, the contract would have to limit the number of outputs in the TX and have a line of code per each output.
Every NFT contract would need to have this boilerplate code, and every transfer would replicate it.

NFTs are essential, not only as tradeable items, but for interaction with other contracts.
For example, they can be used as another contract's access control, where the "main" contract would require the NFT to be spent alongside.
If the NFT would carry the 8-byte state, then it could be used by the main contract as an access counter to limit the number of interactions with each user.

### Native Tokens

The market has already seen a token cambrian explosion, and demand for tokens is seen in growth of [tokens market cap](https://coinmarketcap.com/tokens/views/all/) and market appreciation of native currencies that power them.
The era of tokenization of everything has started with Ethereum and it's only been ramping up.

If only the proposed "carried digest" feature is enabled then it would become possible to implement tokens using a Script contract such as [CashTokens](https://blog.bitjson.com/cashtokens-contract-validated-tokens-for-bitcoin-cash/).
Those tokens would have to be P2SH outputs, and users who want to simply move tokens around would have to worry about nuances of the contract.

Simpler and user-friendly tokens are possible by extending "carried digest" outputs with an amount field and then using consensus rules to enforce token logic over grouped outputs.
Having such tokens means:

- Tokens that are "free" P2PKH citizens and can therefore be locked with any Bitcoin Cash Script.
- Easy for usage, because no new address format required and a single address can receive multiple tokens.
- Easy to upgrade wallets and middleware because token transactions are built almost the same as BCH transactions.
- Easy to parse the blockchain to build databases and track token stats.
- Easy to prove state of token supply, SPV proof sufficient for some kinds.
- Fully compatible with 0-conf and SPV wallets.
- Scaling function, the "big O", unchanged.
- Cheap on node resources, almost as cheap as BCH.
They can be thought of as heavier BCH outputs.
- Fast as BCH, secure as BCH, and almost as cheap to transact with.
- Atomic swaps, decentralized exchange, and token CashFusion possible through existing BCH transaction signing mechanism.

As we will se later, consensus layer will provide a simple interface for token supply management.
The supply is to be controlled using a "singularity" token amount which would act as a positive or negative infinite amount, one that could create or eat token amounts.
Such approach means:

- Possibility of having token supply controlled by a smart P2SH contract placed on singularity token outputs, while letting regular token amounts remain P2PKH.

Having the native token feature would therefore give us the best of both worlds.

### Satoshi Tokens

With "carried digest", "NFT", and "native token" features, satoshi amounts can freely enter and exit the group as long as at least one output of the group is spent in the same transaction.

If one desired to lock up some amount of BCH with the group, he'd have to implement it using Script, which would make moving such amounts within the group inconvenient.
Implementing token logic over satoshi amounts belonging to a group would help with this matter.
The same "native token" logic would be enforced over satoshi amounts instead of token amounts, and the token amount would be used as a boolean to flag an output as a gatekeeper, one which would allow BCH to enter or exit the group.

## Technical Description

[[Back to Contents](#contents)]

Note that, through interaction with various stakeholders of the Bitcoin Cash ecosystem, this proposal has [significantly deviated](#changelog) from past versions which were made to match Andrew Stone's [original proposal](https://docs.google.com/document/d/1X-yrqBJNj6oGPku49krZqTMGNNEWnUJBRFjX7fJXvTs).

### Grouped Output FORMAT

We extend the output format to contain an optional data attachment, inserted as a prefix to the locking script:

*<satoshiAmount\><lockingScriptLength\>*`[PFX_GROUP<groupID>[groupAmount]]`*<lockingScript\>*, where:

- `lockingScriptLength` will count the combined attachment and locking script length,
- `PFX_GROUP` is a constant matching a currently unused and disabled Script opcode `0xEE`,
- `groupID` is a 32-byte group identifier which will have the lowest bit indicate whether outputs have the group amount field,
- `groupAmount` will be present only when `groupID & 0x01 == 0x01`, and be parsed as a [VarInt](https://reference.cash/protocol/formats/variable-length-integer).
Note that outputs of some group may either all have or not have an amount field, there can't be mixing within a group.

We will refer to the full prefix with its arguments as "group annotation".
Unupgraded software, unaware of output format change, will interpret the annotation as part of the locking script.
As a consequence:

- Unupgraded *node* software would fork the blockchain because, from the point of view of unupgraded software, such locking script will be seen as starting with a disabled opcode.
- Unupgraded *non-node* software should already know how to deal with disabled opcodes found on the blockchain, so should not break when encountering them.
From its point of view `0xEE` could be some new data push opcode, followed by random data.

Upgraded node software will cut out the anotation and activate additional transaction verification against [#group-logic-consensus-rules](#group-logic-consensus-rules) before handing the locking script to Script interpreter.

#### Optional PMv3 FORMAT

We extend the [v3 transaction](https://github.com/bitjson/pmv3) format so that each output may contain an optional data attachment, called group annotation:

*<satoshiAmount\><lockingScriptLength\><lockingScript\>*`<groupAnnotationLength>[groupAnnotation]`, where:

- `groupAnnotationLength` is self-descriptive and encoded as [RSN](https://github.com/bitjson/pmv3/blob/master/ranged-script-numbers.md), and
- `groupAnnotation` expands to `<groupID>[groupAmount]`.

Group annotation fields are defined below:

- `groupID` is a 32-byte group identifier which will have the lowest bit indicate whether outputs have the group amount field,
- `groupAmount` will be present only when `groupID & 0x01 == 0x01`, and be parsed as an [RSN](https://github.com/bitjson/pmv3/blob/master/ranged-script-numbers.md).

#### Group TYPE

We will encode the group type using the 1st two LSBs of the `groupID`:

- `groupType | ~0xFFFFULL == 0ULL`
- `groupID & 0xFFFFULL == groupType`.

Lowest bit has already been defined above:

- `HAS_GROUP_AMOUNT = 0x0001`.

Other bits will indicate consensus rules enforced over the group, and are enumerated as:

- `NFT = 0x8000`,
- `NATIVE_TOKEN = 0x4000`, and
- `SATOSHI_TOKEN = 0x2000`.

Allowed group types are enumerated as:

- `CARRIED_DIGEST == 0x0000`,
- `CARRIED_DIGEST | HAS_GROUP_AMOUNT == 0x0001`,
- `NFT == 0x8000`,
- `NFT | HAS_GROUP_AMOUNT == 0x8001`,
- `NATIVE_TOKEN | HAS_GROUP_AMOUNT == 0x1001`,
- `SATOSHI_TOKEN | HAS_GROUP_AMOUNT == 0x2001`.

In the next sections we will define group logic consensus rules for each group type flag.

### Group Annotation Introspection

We will extend [CHIP-2021-02: Native Introspection Opcodes](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md) with unary opcodes for reading group annotation:

- `OP_UTXOGROUPID == 0xCE` Pop the top item from the stack as an input index (Script Number).
From that input, push its prevout `groupID` to the stack in `OP_HASH256` byte order.
- `OP_UTXOGROUPAMOUNT == 0xCF` Pop the top item from the stack as an input index (Script Number).
From that input, push its prevout `groupAmount` to the stack as a Script Number.
Fail if the output doesn't have the `groupAmount` field.
- `OP_OUTPUTGROUPID == 0xD0` Pop the top item from the stack as an output index (Script Number).
From that output, push its `groupID` to the stack in `OP_HASH256` byte order.
- `OP_OUTPUTGROUPAMOUNT == 0xD1` Pop the top item from the stack as an output index (Script Number).
From that output, push its `groupAmount` to the stack as a Script Number.
Fail if the output doesn't have the `groupAmount` field.

### Group Logic Consensus Rules

Existing Bitcoin Cash rules will apply the same to both pure and grouped outputs.
Group logic consensus rules are an *additional* set of rules exclusive to grouped outputs.

#### Group GENESIS

The GENESIS consensus rule will apply to all groups, and will be the only possible way to create the first output of a new group, called the group genesis output.
It is triggered when a `groupID` is found present only on the output side of a transaction.

There can be multiple such outputs in a transaction and each must satisfy:

`groupID == Hash(genData)`, where:

- `Hash` is the double SHA256 hash function, and
- `genData` is byte concatenation of:
    1. Hash of:
        1. Hash of:
            - Transaction header serialized as:
                - version and
                - lock-time.
            - All transaction input's:
                - prevout transaction hash and
                - prevout index.
        2. Hash of all transaction input's:
            - prevout satoshi amount,
            - prevout locking script,
            - prevout group annotation.
        3. Hash of all transaction input's:
            - Hash of unlocking script,
            - sequence number.
        4. Hash of all transaction outputs except group genesis outputs.
    2. Hash of the entire genesis output, omitting the `groupID` field which is being generated.

If the check is triggered and not satisfied, the transaction will fail immediately.

Bitcoin Cash signature preimage format [will be updated](#transaction-signing) to include the group annotation.
Because group `genData` includes input signatures and if the whole annotation were to be always included then it would be impossible to construct a compliant genesis transaction.
Preimage format must account for this and we will exclude `groupID` from the preimage **only if it is a genesis output**.

With the above rule, a genesis transaction is self-evidently valid or invalid and not depending on non-local data.
A new group is created simply by crafting an output that satisfies the rule.
Transaction verification will require `(10 + 2*i + 4*n)` SHA-256 operations, where `n` is the number of genesis outputs appearing in the transaction, and `i` is the number of inputs in the transaction.

To create a compliant transaction which satisfies both TYPE and GENESIS rules, the creator must reroll the genesis output's `groupID = H(genData)` until the 1st two LSB bytes of `groupID` match the desired `groupType`, similarly to how Bitcoin Cash vanity addresses are generated.
Creating a compliant genesis transaction will require `(10 + 2*i + (1ULL<<18)*n)` SHA-256 operations on average, where `n` is the number of genesis outputs appearing in the transaction, and `i` is the number of inputs in the transaction.
This compresses the type information while preserving `groupID` 32 bytes of entropy and also doubles as a light PoW puzzle for group genesis transactions.

Any part of the preimage may be rerolled to set the `groupType` but changing anything in part 1. would reroll `groupID` for all genesis outputs of the transaction.
That leaves us with rerolling parts of the genesis output itself: satoshi amount, the locking script, or group amount.
If using the satoshi amount as a nonce it will cost 0.0013 BCH on average to produce a compliant genesis output, and for most group types it can be reclaimed in the next transaction.
For some group types the BCH will be locked forever, and if this is unacceptable then the creater should have just 1 genesis output in the transaction, and reroll part 1. of the `groupID` preimage.
Alternatively, he may add a `<nonce> OP_DROP` to genesis output's redeem script or reroll the 1st owner address in case a P2PKH genesis output is desired.

Having genesis output's index, and at least one prevout transaction hash and index in the `groupID` preimage ensures that the token genesis output will be unforgable and unique.
In other words, for any `groupID` there can only ever exist exactly one genesis output.
It is so because a duplicate genesis TX would be rejected for double-spending the same output(s).
Crafting 2 transactions using disjoint sets of outputs while rerolling other parts of `groupID` preimages to attempt to find a hash collision is computationally impossible because we're using 256 bits of entropy for `groupID`.
In other words, Wagner's birthday attack on `groupID` is impossible because it will have 128 bits of security against such attack.

Note that the risk of a birthday attack is not the same as with Bitcoin Cash addresses where collisions are possible with the 160-bit address format.
A colliding address pair will have the same owner and is generally not a problem.
A colliding `groupID` could be used to post another genesis TX after it's already been created for that `groupID`, which would destroy confidence in the whole system by disproving the unforgeability conjecture.

Also note that `groupID` preimage includes more data than necessary for group genesis to be unforgeable and unique.
This is by design so that `groupID` can double as an unforgeable cryptographic commitment of transaction data of interest to Script contracts.
Because of how `genData` is structured, Script contracts will be able to prove desired parts of the genesis transaction using fixed size proofs, while other parts will remain compressed in the hash tree structure.
This use is later demonstrated through an [example contract](#unforgeable-covenant-contract).

#### Optional PMv3 GENESIS

With PMv3's "detached proof" feature the input script will already be hashed for inputs that opt-in, and the actual unlocking bytecode will be detached.
Because of that, GENESIS doesn't need to compress individual inputs and the `genData` can be defined as byte concatenation of:

    1. Hash of:
        1. Hash of:
            - Transaction header serialized as:
                - version and
                - lock-time.
            - All transaction input's:
                - prevout transaction hash and
                - prevout index.
        2. Hash of all transaction input's:
            - prevout satoshi amount,
            - prevout locking script,
            - prevout group annotation.
        3. Hash of all transaction input's:
            - unlocking script,
            - sequence number.
        4. Hash of all transaction outputs except group genesis outputs.
    2. Hash of the entire genesis output, omitting the `groupID` field which is being generated.

Group's `genData` will exclude PMv3's detached fields.

#### Transaction Signing

In order to prevent transactions involving grouped outputs from being malleable, we must add the group annotation into Bitcoin Cash signature [preimage format](https://reference.cash/protocol/blockchain/transaction/transaction-signing#preimage-format).

##### Group Annotation Hash

For each grouped transaction output which is NOT a genesis output, append the following information:
- `groupID`, 32 bytes
- `groupAmount` where present, VarInt

Append the following information:
- `countTxGroupGenesis` number of group genesis outputs in the transaction, VarInt

For each group genesis transaction output, append the following information:
- `groupAmount` where present, VarInt

##### Optional PMv3 SIGHASH_DETACHED Hash

Nothing needs changing, because `groupID` excludes the detached fields, so the signature can sign the full TX body, including group genesis outputs and their IDs.

#### Carried Commitment Group

The FORMAT and GENESIS rules will be the only rules for groups of type `0x0000`.
If we don't further constrain the group with additional rules, then it will be allowed to grow from a single genesis output into a BCH sub-DAG where each node will carry the `groupID` set at genesis transaction.

Because the `groupID` is a cryptographic commitment to the genesis transaction, we have named this type "carried commitment".
This type of group can be used to create an unforgeable persistent Script contract which only needs to prove that:

1. Its own locking script matches the locking script set at genesis, which it can do by reconstructing its own `groupID` preimage and filling in the locking script part by introspecting itself.
2. Its own locking script will be placed on all new outputs of the same group, which it can do by using introspection opcodes.

If a carried commitment group is created with the `HAS_GROUP_AMOUNT` flag set, then arbitrary data may be placed in the `groupAmount` field because no rules other than FORMAT and GENESIS would be enforced.
This could be useful for some contracts if they will have a need to record some state of size up to 8 bytes, such as an amount or a counter.

#### NFT Group

This rule will enforce non-fungible token logic over the group.
Such groups will have their `NFT` flag set.

1. If a prevout with `groupType & NFT == NFT` is present then exactly one output of the same `groupID` shall be present.

Note that no rules are enforced on the `groupAmount` field, allowing NFTs to carry a state.
This can be used by contracts to extend the NFT.
For example, a contract could use the field to track the number of interactions with some other contract.

#### Native Token Group

This set of rules will enforce native token logic over the group.
Such groups will have their `NATIVE_TOKEN` flag set.

1. If an output with `groupAmount == 0x00` is present then there must be at least one prevout present with `groupAmount == 0x00`.
2. If there is no `groupAmount == 0x00` prevout, then the `groupAmount` fields must balance for the transaction:
    - `in_groupAmountSum == out_groupAmountSum`.

The 1st check makes zero token amounts behave like an infinite source or sink, able to create or destroy token amounts.
The 2nd check enforces the accounting equation when infinite amounts aren't present.

Singularity token outputs can NOT be destroyed, they may be fanned out and merged but must create at least one singularity "change" output.
Without a singularity output being spent, destroying token amounts is NOT allowed.

However, any grouped output can be permissionlessly removed from the UTXO set by being spent to a grouped `OP_RETURN`, which serves as universal termination.
This is because group consensus logic places no restrictions on outputs locking bytecode so an `OP_RETURN` will not violate amount balancing rules therefore group consensus verification would still be satisfied.

Because an `OP_RETURN` allows `satoshiAmount` to be 0, users will be able to freely clean up their wallets of unwanted tokens and fully reclaim BCH from such token outputs.

#### Satoshi Token Group

This set of rules will enforce native token logic over BCH in the group.
Such groups will have their `SATOSHI_TOKEN` flag set.

1. If an output with `groupAmount == 0x00` is present then there must be at least one prevout present with `groupAmount == 0x00`.
2. If there is no `groupAmount == 0x00` prevout, then the `satoshiAmount` must balance for the transaction:
    - `in_satoshiAmountSum == out_satoshiAmountSum`, and
    - if the `NATIVE_TOKEN` flag is unset then `groupAmount == 0x01` MUST hold true for each output in the transaction.

The 1st check makes zero token amounts allow BCH amounts to enter or exit the group.
The 2nd check enforces the accounting equation over grouped BCH.

This does NOT allow BCH to be created from nothing.
Existing Bitcoin Cash consensus rules will still enforce the accounting equation for satoshi amounts over the whole transaction, regardless of the output type.

#### Disabled Group Types

Allowing other combination of group flags and applying the above rules would result in some duplicate or exotic logic for which use cases have not been demonstrated.

## Specification

[[Back to Contents](#contents)]

{TODO}

## Appendix - Group Logic Pseudo Code

[[Back to Contents](#contents)]

{TO BE UPDATED TO MATCH v4.1}

```
for (each transaction) {
    Verify existing Bitcoin Cash consensus rules
    Verify Group FORMAT and TYPE rules during deserialization

    for (each input in tx.inputs) {
        Verify existing Bitcoin Cash consensus rules
        // Additionally...
        with (input.vout) {
            if (isGrouped()) {
                tx.groups.add(groupID)
                tx.groups(groupID).inputCount++
                if (isNativeToken() || isSatoshiToken()) {
                    if (hasGroupAmount()) {
                        if (isSingularity())
                            tx.groups(groupID).inputSingularity |= true
                        if (isNativeToken())
                            if (tx.groups(groupID).inputGroupSum.safeAdd(groupAmount) == false)
                                fail()
                        if (isSatoshiToken())
                            if (tx.groups(groupID).inputSatoshiSum.safeAdd(satoshiAmount) == false)
                                fail()
                    }
                }
            }
        }
        Verify the rest of existing Bitcoin Cash consensus rules
    }

    for (each output in tx.outputs) {
        Verify existing Bitcoin Cash consensus rules
        // Additionally...
        with (output) {
            if (isGrouped()) {
                tx.groups.add(groupID)
                tx.groups(groupID).outputCount++
                if (isNativeToken() || isSatoshiToken()) {
                    if (hasGroupAmount()) {
                        if (isSingularity())
                            tx.groups(groupID).outputSingularity |= true
                        if (isNativeToken())
                            if (tx.groups(groupID).outputGroupSum.safeAdd(groupAmount) == false)
                                fail()
                        if (isSatoshiToken()) {
                            if (isNativeToken() == false)
                                if (groupAmount > 1)
                                    fail()
                            if (tx.groups(groupID).outputSatoshiSum.safeAdd(satoshiAmount) == false)
                                fail()
                        }
                    }
                }
            }
        }
        Verify the rest of existing Bitcoin Cash consensus rules
    }

    // Group consensus logic
    for (each group in tx.groups) {
        with (group) {
            if (inputCount == 0) {
            // Group genesis
                if (outputCount > 1)
                    fail()
                if (tx.groupPreimageCommon.exists == false)
                    tx.groupPreimageCommon.create()
                if (Hash(tx.groupPreimageCommon + genesisPreimage()) != groupID)
                    fail()
            }
            else {
                if (isNativeToken() || isSatoshiToken()) {
                    if (hasGroupAmount()) {
                        if(inputSingularity != outputSingularity)
                            fail()
                        if(inputSingularity == false) {
                            if(isNativeToken == true)
                                inputGroupSum == outputGroupSum
                            if(isSatoshiToken == true)
                                inputSatoshiSum == outputSatoshiSum
                        }
                    }
                    else {
                        if (inputCount != 1 || outputCount != 1)
                            fail()
                    }
                }
            }
        }
    }

    Verify the rest of existing Bitcoin Cash consensus rules
}
```

## Appendix - Group Logic Flowchart

[[Back to Contents](#contents)]

{TODO}

## Appendix - Example Uses

[[Back to Contents](#contents)]

{TO BE UPDATED TO MATCH v4.1}

**Shorthand notation**

```
<TX No.> <TX Description>
    In
        // comment
        [satoshiAmount] // pure BCH output
        [satoshiAmount, groupID] // amountless grouped output
        [satoshiAmount, groupID, groupAmount] // grouped output
        ...
    Out
        ...
    Fee [satoshiAmount]
```

### Example Uses Contents

- [Essential Operations Examples](#essential-operations-examples)
    - [Genesis Example](#genesis-example)
    - [Mint Example](#mint-example)
    - [Melt Example](#melt-example)
    - [Transfer Example](#transfer-example)
    - [Permissionless Burn Example](#permissionless-burn-example)
    - [Fixed Supply Token Example](#fixed-supply-token-example)
    - [NFT Example](#nft-example)
- [Token CoinJoin Examples](#token-coinjoin-examples)
    - [Atomic Swaps](#atomic-swaps)
    - [Decentralized Exchange](#decentralized-exchange)
    - [CashFusion](#cashfusion)
- [Example Script Contracts](#example-script-contracts)
    - [Unforgeable Covenant Contract](#unforgeable-covenant-contract)
    - [Sibling Metadata Contract](#sibling-metadata-contract)

### Essential Operations Examples

[[Back to Example Uses Contents](#example-uses-contents)]

#### Genesis Example

[[Back to Example Uses Contents](#example-uses-contents)]

Genesis transaction creates a new token group by creating the first token group's output, called the group genesis output.
It is created right off the bat, by spending some BCH and creating an output compliant with the [GENESIS rule](#token-group-genesis).

```
01 Genesis transaction
    In
        [100000]
    Out
        [ 10000, 0x0110...ABCD, 0]
            // Native token group type, has amount field, singularity amount
            // P2SH output, redeem script: `<nonce> OP_DROP {P2PKH TEMPLATE}`
        [989000]
            // BCH change
    Fee [  1000]
```

Recall that the first two LSBs of the `groupID` will encode the token's type.
To create a valid group genesis output, the TX creator must reroll the `groupID` preimage until it matches.
In this example we placed a `nonce` inside a P2SH redeem script.
Alternatively it could be P2PKH where we would use the satoshi amount as nonce or, if we're generating only one genesis output, another output would do, e.g. `<nonce> OP_RETURN`.

#### Mint Example

[[Back to Example Uses Contents](#example-uses-contents)]

When the genesis output has been created, token amounts can be minted simply by spending the singularity output and creating any number of ordinary token outputs alongside one or more singularity "change" outputs.
For easier supply auditing it may be desireable that the sigularity amount output behaves like an NFT.
Such behavior can be enforced by [using a Script covenant contract](#unforgeable-covenant-contract).

```
02 Mint transaction
    In
        [ 10000, 0x0110...ABCD, 0]
    Out
        [  2000, 0x0110...ABCD, 100000000]
        [  2000, 0x0110...ABCD, 100000000]
        [  2000, 0x0110...ABCD, 100000000]
        [  3000, 0x0110...ABCD, 0]
    Fee [  1000]
```

Notice how the BCH amount held by the singularity output is distributed to ordinary token outputs and also used to pay for the TX fee.

#### Melt Example

[[Back to Example Uses Contents](#example-uses-contents)]

We can destroy token amounts the same way, an operation which we call "melt" to disambiguate from [permissionless burn](#permissionless-burn-example) operation shown later.

```
03 Melt transaction
    In
        [  3000, 0x0110...ABCD, 0]
        [  2000, 0x0110...ABCD, 100000000]
    Out
        [  4000, 0x0110...ABCD, 0]
    Fee [  1000]
```

Notice how the BCH amount is reclaimed while token amounts are "eaten" by the singularity.

#### Transfer Example

[[Back to Example Uses Contents](#example-uses-contents)]

After some supply has been minted, the issuer can distribute their tokens to users.

```
03 Transfer transaction
    In
        [  2000, 0x0110...ABCD, 100000000]
    Out
        [   700, 0x0110...ABCD,   1000000] // USDT token sent to Bob
        [   700, 0x0110...ABCD,  99000000] // Token change
    Fee [   600]
```

With the above transaction Bob has received some USDT directly from the issuer.
Now Bob wants to send some to Alice.
Because BCH amount has been depleted close to dust limit, he needs to add more BCH in order to create more token outputs and pay for the fee.

```
04 Bob to Alice
    In
        [   700, 0x0110...ABCD,   1000000] // Bob's USDT token
        [100000] // Bob's BCH
    Out
        [  3000, 0x0110...ABCD,    200000] // USDT token sent to Alice
        [  3000, 0x0110...ABCD,    800000] // Bob's token change
        [ 93700] // Bob's BCH change
    Fee [  1000]
```

With the above transaction Bob has sent some of his USDT to Alice and recharged those token outputs with some more BCH.

#### Permissionless Burn Example

[[Back to Example Uses Contents](#example-uses-contents)]

The issuer went bust and now its USDT tokens are worthless.
Users may want to clean up their wallets of unwanted tokens while reclaiming the BCH that was stored in them.

```
05 Alice burning tokens
    In
        [  3000, 0x0110...ABCD, 200000] // Alice's USDT
    Out
        [     0, 0x0110...ABCD, 200000] // Sent to `OP_RETURN`
        [  2000] // Alice's BCH change
    Fee [  1000]
```

Note that existing consensus rules allow the BCH amount of an `OP_RETURN` output to be 0.
This way token and BCH amounts will still balance for the transaction, but the token amount will be permanently removed from the UTXO set.

#### Fixed Supply Token Example

[[Back to Example Uses Contents](#example-uses-contents)]

It is created right off the bat, by spending some BCH and creating a genesis output of a non-singularity amount.

```
01 Fixed supply token genesis transaction
    In
        [100000]
    Out
        [ 10000, 0x0110...ABCD, 2100000000000000]
            // Native token group type, has amount field, non-singularity amount
        [989000]
            // BCH change
    Fee [  1000]
```

Because a singularity output hasn't been created at genesis, it will be impossible to create later.

#### NFT Example

[[Back to Example Uses Contents](#example-uses-contents)]

It is created right off the bat, by spending some BCH and creating an amountless genesis output.

```
01 NFT genesis transaction
    In
        [100000]
    Out
        [ 10000, 0x0010...ABCD]
            // Native token group type, amoutless
        [989000]
            // BCH change
    Fee [  1000]
```

When the group is of NATIVE_TOKEN type, consensus rules will enforce that there exists exactly 1 amountless UTXO of that group.

### Token CoinJoin Examples

[[Back to Example Uses Contents](#example-uses-contents)]

#### Atomic Swaps

[[Back to Example Uses Contents](#example-uses-contents)]

Group tokenization allows mixing multiple token and BCH TXOs in a single transaction.
This enables any two parties to perform an atomic swap using existing Bitcoin
Cash transaction signing features.

If trading parties already discovered each other through some communication
channel they can agree in advance on all outputs and then each sign those outputs using `SIGHASH_ALL | SIGHASH_ANYONECANPAY` and construct a transaction like the below.

```
Atomic Swap Transaction
    In
        [101000] // BCH signed by Bob
        [   546, Token A, 50000000] // Token A signed by Alice
    Out
        [100000] // BCH to Alice's address
        [   546, Token A, 50000000] // Token A to Bob's address
    Fee [  1000]
```

This allows for multi-token and multi-party trades, too.
The below can be done in the same way, as long as everyone can coordinate and agree on outputs.

```
Multi-token and multi-party Atomic Swap Transaction
    In
        [ 10000] // BCH signed by Alice
        [500000] // BCH signed by Bob
        [ 20000] // BCH signed by Carol
        [   546, Token A, 20000000] // Token A signed by Alice
        [   546, Token C, 10000000] // Token C signed by Carol
    Out
        [107908] // BCH to Alice's address (Trade 1 & BCH change)
        [117908] // BCH to Carol's address (Trade 1 & BCH change)
        [299000] // Bob's change (Trade 1)
        [   546, Token A,  2000000] // Token A to Bob's address (Trade 1)
        [   546, Token C,  1000000] // Token C to Bob's address (Trade 1)
        [   546, Token A,  4000000] // Token A to Carol's address (Trade 2)
        [   546, Token C,  2000000] // Token C to Alice's address (Trade 2)
        [   546, Token A, 14000000] // Alice's token change (Trade 1 & 2)
        [   546, Token C,  7000000] // Carol's token change (Trade 1 & 2)
    Fee [  3000] // All participants agree to split the fee
```

Here Bob is buying two tokens at the same time and is paying 1/3 of the transaction fee.
Alice and Carol are trading with Bob (Trade 1) and with each other (Trade 2) at the same time.
Alice and Carol each pay for their share of the fee and for creation of additional token UTXOs.

The above transaction could be made more compact by merging BCH and token TXOs into token TXOs.
The outputs would then look like this:

```
    Out
        [299546, Token A,  2000000] // Bob's address
        [   546, Token C,  1000000] // Bob's address
        [108454, Token C,  2000000] // Alice's address
        [   546, Token A, 14000000] // Alice's address
        [118454, Token A,  4000000] // Carol's address
        [   546, Token C,  7000000] // Carol's address
```

#### Decentralized Exchange

[[Back to Example Uses Contents](#example-uses-contents)]

It is possible is to create a blind offer by crafting a partial transaction and signing an input using `SIGHASH_SINGLE | SIGHASH_ANYONECANPAY`.
Suppose Bob wants to buy a token and wants to post a bid offer.
He will prepare a partial transaction like below and publish it to some public message board or a DEX server.

```
Blind Offer Partial Transaction
    In
        [100000] // BCH signed by Bob
    Out
        [   546, Token T, 30000000] // Token T to Bob's address
```

Then, someone sees that offer and decides to take it.
The taker will complete the transaction by: adding his token to the inputs, adding outputs to take the offered BCH and his own token change, and having the token input sign the transaction using `SIGHASH_ALL`.

```
Taker Completed Transaction
    In
        [100000] // BCH signed by Bob, SIGHASH_SINGLE | SIGHASH_ANYONECANPAY
        [   546, Token T, 50000000] // Token T signed by Taker, SIGHASH_ALL
    Out
        [   546, Token T, 30000000] // Token T to Bob's address
        [   546, Token T, 20000000] // Token T change back to Taker's address
        [ 98454] // BCH to Taker's address
    Fee
        [  1000]
```

The taker then posts the transaction to the blockchain and with that the trade is completed.
Should Bob change his mind, he can pull the offer by spending the same BCH output that's part of the offer.

```
Blind Offer Revocation Transaction
    In
        [100000] // BCH signed by Bob
    Out
        [ 99000] // BCH to Bob's address
    Fee
           1000 // Miners earn the fee even on cancelled trades :)
```

#### CashFusion

[[Back to Example Uses Contents](#example-uses-contents)]

{TODO}

### Example Script Contracts

[[Back to Example Uses Contents](#example-uses-contents)]

#### Unforgeable Covenant Contract

[[Back to Example Uses Contents](#example-uses-contents)]

Recall that a `groupID` is generated by hashing a preimage committing to that group's entire [genesis transaction](#group-genesis).
Therefore, as a minumum, an unforgeable covenant contract needs to prove that:

- It matches the group's genesis output contract, which is conveniently part of the `groupID` preimage.
- It could have been set only at genesis, i.e. no descendant could have freed itself from the covenant to then later create another instance.

Redeem script:  
`OP_INPUTINDEX OP_UTXOBYTECODE OP_0 OP_OUTPUTBYTECODE OP_EQUAL` Verify that
the script is carried forward to index-0 output.  
`OP_INPUTINDEX OP_UTXOGROUPID OP_0 OP_OUTPUTGROUPID OP_EQUAL OP_AND` Verify that index-0 output belongs to the same group.  
`OP_SWAP` move signature `genDataHeader` to top of stack.  
`OP_INPUTINDEX OP_UTXOBYTECODE OP_CAT OP_0 OP_CAT OP_HASH256` Reconstruct `groupID` using current script and singularity amount.  
`OP_0 OP_OUTPUTGROUPID OP_EQUAL OP_AND` Verify that it matches the current group and with that verify it matches the group's genesis script.  
`OP_1 OP_OUTPUTGROUPID OP_0 OP_EQUAL`  
`OP_1 OP_OUTPUTGROUPAMOUNT OP_0 OP_NUMNOTEQUAL OP_OR OP_AND` Verify that index-1 is either a pure BCH output or a non-singularity grouped output.  
`OP_SWAP OP_TXOUTPUTCOUNT OP_2 OP_EQUAL OP_AND` Verify that no other outputs are being created, therefore a singularity output can't be hidden from the script.  
`[...]OP_AND` Any other conditions, such as a public key signature of the owner.

Signature:  
`[...]<genDataHeader>`

The `genDataHeader` is part of the `groupID` preimage preceding the genesis output's `lockingScript`, and it must be provided by the spender.

The above covenant boilerplate code proves itself and enforces NFT behavior on the singularity 0-amount token output.
Fanning out and merging could be enabled by coding in more slots for singularity outputs.

The size of the above code is given below:

    6   Signature push opcodes (3+1+1+1)
    1   OP_0
    38  Redeem script boilerplate code
    40  Unforgeable covenant proof part of signature
    ------------------------------------
    85

The above script could be extended to enforce conditions under which:

- BCH will be able to enter or escape the covenant
- token amounts can be minted
- token amounts can be destroyed

Enforcing BCH and token rules simultaneously could require the amount of BCH paid into/out-of the covenant to match the amount of tokens created/destroyed.
Such tokens would be trustlessly backed by BCH and remain free P2PKH citizens because amounts would be allowed to exist outside the covenant only if the required amount of BCH would be locked inside the covenant.

#### Sibling Metadata Contract

[[Back to Example Uses Contents](#example-uses-contents)]

Assuming that the metadata contract group will be of NFT group type, i.e. no amount field and can't fan out.

Redeem script:  
`OP_DUP`
*g g s1 s2 m*  
`OP_ROT`
*g s1 g s2 m*  
`OP_INPUTINDEX OP_UTXOBYTECODE OP_1 OP_OUTPUTBYTECODE OP_EQUAL`
*1 g s1 g s2 m* Verify that the metadata contract is carried forward to index-1 output.  
`OP_1 OP_OUTPUTBYTECODE OP_DATA_1 OP_RETURN OP_EQUAL OP_OR`
*1 g s1 g s2 m* Allow the contract to be terminated using an OP_RETURN.  
`OP_INPUTINDEX OP_UTXOGROUPID OP_1 OP_OUTPUTGROUPID OP_EQUAL OP_AND`
*1 g s1 g s2 m* Verify that index-1 output belongs to the same group.  
`OP_ROT`
*g s1 1 g s2 m*  
`OP_SWAP OP_0 OP_OUTPUTBYTECODE OP_SWAP OP_CAT OP_0 OP_SWAP OP_CAT OP_SWAP`  
`OP_CAT OP_HASH256`
*h1 1 g s2 m* Reconstruct target `groupID` using current script and singularity amount.  
`OP_0 OP_OUTPUTGROUPID OP_EQUAL OP_AND`
*1 g s2 m* Verify that it matches the target group and with that verify it matches the target group's genesis script.  
`OP_ROT` *g s2 1 m*  
`OP_SWAP OP_1 OP_OUTPUTBYTECODE OP_SWAP OP_CAT OP_0 OP_SWAP OP_CAT OP_SWAP`  
`OP_CAT OP_HASH256`
*h2 1 m* Reconstruct metadata `groupID` using current script and singularity amount.  
`OP_1 OP_OUTPUTGROUPID OP_EQUAL OP_AND`
*1 m* Verify that it matches the metadata group and with that verify it matches the metadata group's genesis script.  
`OP_SWAP OP_POP`
*1* Pop the metadata from the stack.  

Signature:  
`<Metadata1><genSatsTarget><genSatsMe><genDataCommon>`

The above contract can only be spent if the target group's singularity output is spent in the same transaction.

## Activation Costs and Risks

[[Back to Contents](#contents)]

{TODO section is incomplete, still a draft}

The solution is conceptually simple, which means that anyone new to the solution should not have much trouble understanding it, and then following the steps to implement it.
The entire implementation showcasing it can fit into a single `.cpp` file consisting of a single function using simple language structures and which can fit well under ~400 lines of C++.
This number comes from an actual implementation that was focused on understandability, not succinctness.
By reusing existing loops and other techniques, it can be implemented in even fewer lines.
This kind of solution is easy to implement and reason about.
We cannot invent problems with this approach just to satisfy the reader looking for problems.
If we have missed something in our reasoning, we will be happy to hear from you.

### Costs

The cost is one-off workload increase for node developers, after which the tokens will continue to provide utility even if their consensus layer would be frozen.
Even as such, tokens would automatically benefit from other upgrades such as new Script opcodes, so the benefits could increase in the future even with group tokenization itself remaining unchanged.

Downstream, middleware developers can rely on the same design patterns as Bitcoin uses which have been successfully tested since 2009.

{add a note re. possible output sizes}

### Risks

If we define risk as [probability times severity](https://en.wikipedia.org/wiki/Risk#Expected_values) then we could make an estimate using the [risk matrix](https://en.wikipedia.org/wiki/Risk_matrix).

Given the layer on which this is to be implemented, the probability of bugs and network splits is entirely controlled by node developers and quality of their work.
We don't recall a case where different node implemetations disagreed about the way to balance the BCH amount of a transaction and we think it's reasonable to expect that it shouldn't happen with group amounts, either.
After the node software QC process is completed, the probability of a bug should be very low.
If deemed necessarry, formal verification should be possible.

In case of a problem, the severity would depend on which node implementation forked off i.e. their hashpower proportion.
The consequence should be temporary unavailability of the network to everyone relying on that node implementation, and it is in the nature of such events that they can be detected and rectified quickly.

## Ongoing Costs and Risks

[[Back to Contents](#contents)]

{TODO section is incomplete, still a draft}

We don't see a significant increase in ongoing costs and risks.
Node implementation should have been verified watertight before activation, and ongoing costs are then moved to middleware which will start to use the building blocks provided.
The risk of wrongful middleware implementation would mostly have the consequence of not seeing the funds, and it would be really hard to actually lose the funds.

On consensus layer, some consideration will have to be given to existence of group code.

Listed below are some changes that would not be significantly affected by having group tokens:

- Network rules, unless we choose to give special treatment to group UTXOs with regards to fee or dust limit requirements
- Blocksize changes, unless we choose to have a separate cap for token TXes
- Pruning
- New opcodes
- New SIGHASH
- New signature types
- UTXO commitments

and some which might be:

- Changing numerical precision for BCH & tokens, and it should be possible to change BCH precision independently of tokens
- Totally new state scheme e.g. [utreexo](https://dci.mit.edu/utreexo)
- ?

## Implementations

[[Back to Contents](#contents)]

- [{WIP} BCHN](https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/merge_requests/1203)

For reference, original implementation with a broader set of features:

- [BU implementation](https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/0d8b59d47e954a6bf40e5a031129924f5edd1914/src/consensus/grouptokens.cpp)
- [BU demo](https://www.nextchain.cash/groupTokensCliExample)

## Test Cases

[[Back to Contents](#contents)]

{TODO}

For reference, tests for the original implementation:

- [BU/grouptoken_tests.cpp](https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/0d8b59d47e954a6bf40e5a031129924f5edd1914/src/test/grouptoken_tests.cpp)
- [BU/grouptokens.py](https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/0d8b59d47e954a6bf40e5a031129924f5edd1914/qa/rpc-tests/grouptokens.py)

## Security Considerations

[[Back to Contents](#contents)]

{TODO section is incomplete, still a draft}

Not even a single line of existing consensus code needs to be changed to implement Group Tokenization.
So implementations should be able to provably show that the addition of Group Tokenization cannot affect the security of the BCH token.

Group Tokenization changes do not add any additional network messages, so do not increase the network attack surface.

Improper implementations of Group Tokenization could cause a blockchain hard fork.
This is true of any miner-validated tokenization system, or of any new feature requiring a hard fork.

## Evaluation of Alternatives

[[Back to Contents](#contents)]

{TODO section is incomplete, still a draft}

We see not having the feature as the only alternative.
Other token proposals are implemented with smart contracts therefore not on the same utility level as native currency and are conceptually different from how the native currency operates.
Our proposal conceptually operates the same way as native currency so downstream developers should not have too much trouble implementing it if they're already familiar with how Bitcoin works.

Other smart contract proposals could operate on grouped tokens, and therefore benefit from grouped tokens, because group tokens provide built-in nouns and a select set of low level verbs.

More generally, we see alternatives in only the rollout approach:

1. We enable native tokens through this proposal,
2. Smart contract solutions either roll their own tokens or use native tokens;

or a simultaneous rollout, or:

1. Roll out a smart contract platform that can roll their own tokens,
2. Wait for the market to demand something more efficient, and then enable native tokens.

The latter approach has the biggest cost in:

- Missed opportunity in the time it takes to build smart contract token systems and evaluate the market response.
- Difficulty in consistently identifying and displaying different smart-contract based tokens in user wallets.
- Technical debt of downstream users having to adapt to new design patterns introduced by the smart contract platform.

We can wait for the market, but the market won't wait for us.
We believe it's reasonable to anticipate that demand if Bitcoin Cash is to get ahead.

We see simultaneous rollout as the preferred option because then synergetic benefit could be achieved right from the start if new smart contracts features would be designed in such way that they can fully use group tokens.

For this to happen, we invite other teams to consider that approach at this early stage in the upgrade cycle, and then a set of features more powerful than any single team could deliver on their own could become available on Bitcoin Cash.

### Industry Acceptance

#### IOTA

The proposed [IOTA tokenization](https://blog.iota.org/tokenization-on-the-tangle-iota-digital-assets-framework/) is a functional subset of Group Tokenization, but notably uses the exact same technique of annotating UTXOs with token identifiers.

#### ION

[ION tokens](https://ionomy.com/) are ported directly from Andrew Stone's work.

#### Blockstream Liquid

["Confidental Assets"](https://blockstream.com/bitcoin17-final41.pdf) uses the same general approach of "...attaching to each output an asset tag identifying the type of that asset, and having verifiers check that the verification equation holds for subsets of the transaction which have only a single asset type."

## Stakeholder Cost and Benefits Analysis

[[Back to Contents](#contents)]

We present a stakeholder taxonomy.
Everyone should be able to find themselves within at least one group.
We attempt to evaluate the impact of this development on all stakeholders and we invite you to join us, add your names to this list, and comment whether you agree with our assesment of the impact.
We're happy to address any concerns or questions you may have.

General costs and benefits:

- (+) Competitive advantage in comparison to other blockchains/ecosystems.
- (+) This functionality will enable competition from within our ecosystem.
Some may feel threatened by this, but what is the alternative?
To have the competition build outside of our ecosystem and let them create value elsewhere?
- (+) Attracting new users, talent, and capital from outside thus helping grow the entire ecosystem centered around Bitcoin Cash.
Conceptually simple tools mean more potential builders.
- (+) Opportunity cost.
Success of tokens is a good measure of that.
Missed "revenue" in terms of user, talent, and capital inflows.
Missed ecosystem growth opportunity.
- (+) Future proof.
Since every group token is also a BCH output, it should seamlessly work with everything that can work with BCH, including Script and any future smart contract platform.
- (+) Every grouped output created also creates marginal demand for BCH.
- (+) Increased Bitcoin Cash adoption and utility, as the "first among equals" currency that powers the Bitcoin Cash blockchain.
- (+) Low risk to native currency security, as consensus rules are only added in
a self-contained way and toggled through a single opcode.
Existing rules are not changed.
- (-) Potential to speed up adoption.
While this benefits BCH, it could create problems of its own.
We don't see adoption creating problems as a realistic outcome, but this perspective is worth consideration, and it would be a good problem to have.

### 1 Node Stakeholders

- (-) Marginally more resource usage when compared to the simplest BCH UTXO.

#### 1.1 Miners

- (+) More fee revenue through increased demand for transactions.

#### 1.2 Big nodes

Exchanges, block explorers, backbone nodes, etc.

- (+) Easy to implement other currencies through same familiar design patterns that are now used for Bitcoin Cash alone.

#### 1.3 User nodes

Hobbyiest full nodes.

#### 1.4 Node developers

- (-) One-off workload increase
- (+) Later it just works out of the box, downstream developers create utility.

### 2 Userspace stakeholders

- (+) Barrier to entry to token usage lowered.

#### 2.1 Business users

- (+) Access to financial instruments previously not available, or access to them at a better price and quality than competition can provide.
- (+) Enables potential new business models.

#### 2.2 Userspace developers

- (+) Easy to implement other currencies through the same familiar design patterns that are now used for Bitcoin Cash alone.
- (+) With more growth there will be more opportunity for professional advancement within the ecosystem.

#### 2.3 Individual users

- (+) Get to enjoy the best peer-to-peer electronic cash system!

### 3 Holders and speculators

#### 3.1 Long

- (+) More adoption correlates with increase in price.
- (+) Even other tokens marginally increase demand for BCH, so the more
transactions of any kind, the better.

#### 3.2 Short

- (+) More liquidity in the market. Following Bitcoin Cash may help you decide to switch side and benefit from it!

### 4 Opinion holders who are not stakeholders

We are hoping developments like this will move you towards becoming stakeholders!

## Statements

[[Back to Contents](#contents)]

[2021-04-02 General Protocols Statement on "CHIP-2021-02 Group Tokenization for Bitcoin Cash"](https://read.cash/@GeneralProtocols/general-protocols-statement-on-chip-2021-02-group-tokenization-for-bitcoin-cash-e7df47a0)

>A lot of work and research has gone into the technical specification of this CHIP. However, in its current form, GP does not think it is simple / minimal enough to be considered for acceptance into the network. Furthermore, important sections about costs, risks and alternatives are incomplete or have wrongfully been asserted as non-existent. GP would like to see a minimum viable change isolated from the current large scope, and then a deeper evaluation of costs, risks and alternatives.

[2021-04-05 Burak, SwapCash](https://youtu.be/f9wTZzNk1ro?t=4319)

>GROUP tokens are by far the most streamlined proposal to achieving token-supported covenants which could definitely help Bitcoin Cash unlocking its potential to mass adoption. Group tokens are not only needed for gaining user adoption, but also developer adoption. Builders need more scripting capabilities to building meaningful dapps of which Group proposal could definitely contribute to.

[2021-04-06 MaxHastings, Mint Slp](https://bitcoincashresearch.org/t/chip-2021-02-group-tokenization-for-bitcoin-cash/311/29?u=bitcoincashautist)

>As a developer and co-founder of Mint SLP https://mintslp.com/ 1

>I believe miner validated tokens is a smart step in the right direction! The Group Token proposal seems like a big improvement over SLP.

>Thank you @andrewstone and others involved for working on and pushing this feature into BCH.

>I hope the community can come together to support the Group Token proposal or something similar! You have my support for what it is worth!

## Future Work

[[Back to Contents](#contents)]

[Full future functional specification and upgrade path.](https://docs.google.com/document/d/1X-yrqBJNj6oGPku49krZqTMGNNEWnUJBRFjX7fJXvTs/edit)

## Discussions

[[Back to Contents](#contents)]

[CHIP Discussion](https://bitcoincashresearch.org/t/chip-2021-02-group-tokenization-for-bitcoin-cash/311)

[Previous Discussion](https://bitcoincashresearch.org/t/native-group-tokenization/278)

## Changelog

[[Back to Contents](#contents)]

This section summarizes the evolution of this document.

**2021-12-08** [current](#) 4.1 Tweak GENESIS and add NFT group flag

- Add a NFT group flag so we can have NFTs that carry some state
- Tweak GENESIS to compress each input in the preimage
- Add PMv3 GENESIS and SIGHASH sections
- Wording

**2021-11-29** [b96c2f35](https://gitlab.com/0353F40E/group-tokenization/-/commit/b96c2f35dd551e7fa9e8420d19d72e45bfeac50c) 4.0 Unforgeable Groups

- Simplified the output format and consensus layer
- Generalized output groups as "carried digest", having only the GENESIS rule enforced
- Group amount field is now optional and indicated using the LSB of `groupID` so the whole group must have or not have it
- Native token groups and Satoshi token groups (aka "holds BCH") are then a more restricted variant of a generalized group, having token logic enforced over respective amount fields, and NFT logic enforced over amountless token groups.
- 0-amount indicates "singulary", i.e. an infinite source or an infinite sink which allows token amount creation and destruction
- Any advanced features, such as metadata updating, are to be implemented using unforgeable Script covenants, as demonstrated in the examples section
- Reworked CHIP sections

**2021-11-11** [2904667d](https://gitlab.com/0353F40E/group-tokenization/-/commit/2904667daa32f0cf9dce862c916ea6305f2fdf88) 3.1 Sections Overhaul

- Added back MELT and infinite MINT
- Tweaked authority-type NFTs
- Tweaked the tokenID preimage to enable Script covenants
- Renamed "baton" back to "authority", renamed metadata LOCK_X to EDIT_X
- Reworked CHIP sections: made motivation more concise, added intro section intended for wider audience, added contents, changelog, reworked technical description, updated examples, added example Script covenant contract...

**2021-05-09** [8c9cb4a2](https://gitlab.com/0353F40E/group-tokenization/-/blob/8c9cb4a23364e3bba3a87319f1ad068065735530/CHIP-2021-02_Group_Tokenization_for_Bitcoin_Cash.md) 3.0 One Token Standard

- Result of brainstorming with Emil Oldenburg
- Authority system restricted to exactly one UTXO per token
- Introduced metadata locks

**2021-04-17** [4be89a3b](https://gitlab.com/0353F40E/group-tokenization/-/blob/4be89a3b7b10329536bbd3367b87e2405df39e81/CHIP-2021-02_Group_Tokenization_for_Bitcoin_Cash.md) 2.0 Full Overhaul

- Original scope sliced down in response to feedback received
- All advanced features removed
- Reworked technical description to flow better
- Included group consensus rules flowchart

**2021-03-14** [fa4964d2](https://gitlab.com/0353F40E/group-tokenization/-/blob/fa4964d2099c0e62423b8f392eabeb25df8518bc/CHIP-2021-02_Group_Tokenization_for_Bitcoin_Cash.md) 1.1 Completed the First Draft

- Copied Andrew Stone's [specification](https://github.com/bitcoin-unlimited/BUwiki/commit/880d69330623e4eb4e0b068c7361c6c60494e9e7) into the CHIP body.
- Reworked technical description to include advanced Group features: subgroups, hold BCH, and covenant.

**2021-02-23** [646232cc](https://gitlab.com/0353F40E/group-tokenization/-/blob/646232cc628e65bc624ca8a455d17e3db16bfbd8/CHIP-2021-02_Group_Tokenization_for_Bitcoin_Cash.md) 1.0 First Draft, CHIP [published](https://bitcoincashresearch.org/t/chip-2021-02-group-tokenization-for-bitcoin-cash/311/1) on Bitcoin Cash Research forum

- CHIP created to describe Andrew Stone's [original specification](https://github.com/bitcoin-unlimited/BUwiki/blob/e9f1956657c375b6eb2c5b85b5b1c868e5db839c/grouptokenization/groupbchspec.md).
- First draft technical description incomplete, introduced only the token transfer, authority, and genesis features.

## Copyright

[[Back to Contents](#contents)]

To the extent possible under law, this work has waived all copyright and related or neighboring rights to this work under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).

## Credits

[[Back to Contents](#contents)]

Satoshi Nakamoto, invention of [Bitcoin: A Peer-to-Peer Electronic Cash System](https://www.bitcoin.com/bitcoin.pdf).  
Andrew Stone, authoring the [original Group Tokenization proposal](https://www.nextchain.cash/grouptokens) and full showcase implementation.

The CHIP however, is really authored by everyone who's interacted with it.
I'd like to personally thank:

- Andrew Stone, for inspiration, being supportive of the initial CHIP versions, and private consultations,
- Thomas Zander, for inspiration, being the "first responder" to the proposal, and helping me realize the importance of attention to details and their impact on the full product,
- matricz, for the [inspiring post](https://read.cash/@mtrycz/how-i-envision-decentralized-cooperation-on-bitcoin-cash-9876b9e9) on how decentralized cooperation should work,
- imaginary_username, for looking at it with a critical mind and helping me find a better way forward,
- BigBlockIfTrue, for early interaction with the proposal and explaining to me the nuances of locking and unlocking script,
- emergent_reasons, for setting the bar high for CHIPs and asking for steel-man arguments,
- Calin Culianu, for kicking the CHIP into higher gear with the PFX_GROUP approach,
- Emil Oldenburg, for having the patience to help me see the advantage of having a single authority and enforcing metadata locks.

and everyone else who interacted with this proposal and by doing so helped bring it to the level where it currently is.
If I forgot someone, please let me know!

Sorted alphabetically: AsashoryuMaryBerry, Burak, Estebansaa, FerriestaPatronum, Fixthetracking, Freedom-phoenix, Freetrader, George Donelly, Jason Dreyzehner, Jonathan Silverblood, Licho, Marc De Mesel, MobTwo, Mr. Korrupto, Saddit42, Shadow Of Harbringer, Supremelummox, Tl121, Tula_s, Twoethy.
